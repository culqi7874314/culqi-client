import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ErrorMessage from '../ErrorMessage.vue'

describe('ErrorMessage', () => {
  it('renders properly', () => {
    const wrapper = mount(ErrorMessage, { props: { message: 'Test message' } })
    expect(wrapper.text()).toContain('Test message')
  })
})
