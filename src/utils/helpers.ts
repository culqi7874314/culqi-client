import { FirstCardNumbers } from "./constants";

export const validateCardType = {
    amex : (cardNumber: string) => cardNumber.charAt(0) === FirstCardNumbers.amex,
    visa : (cardNumber: string) => cardNumber.charAt(0) === FirstCardNumbers.visa,
    mastercard : (cardNumber: string) => cardNumber.charAt(0) === FirstCardNumbers.masterCard,
};

export const copyToClipboard = (text: string) =>{
    navigator.clipboard.writeText(text);
};
