import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/LoginView.vue'
import Payment from '../views/PaymentView.vue'
import Verification from '../views/VerificationView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/pago',
      name: 'payment',
      component: Payment,
      meta: { requiresAuth: true }
    },
    {
      path: '/consulta',
      name: 'verification',
      component: Verification,
      meta: { requiresAuth: true }
    },
    { path: '/:pathMatch(.*)*', redirect: '/login' }
  ]
})

router.beforeEach((to, from, next) => {
  const isAuthenticated = localStorage.getItem('commerceId');
  if (to.matched.some(record => record.meta.requiresAuth) && !isAuthenticated) {
    next({ name: 'Login' });
  } else {
    next();
  }
});

export default router
