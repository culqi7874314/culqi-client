/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}", ],
  theme: {
    extend: {
      colors: {
        brand: {
          purple: '#763383',
          purple_light: '#CA93D5',
          green: '#00918E',
          green_light: '#02c9c5',
          orange: '#E07523',
          orange_light: '#E79A00'
        }
      }
    },
  },
  plugins: [],
}

