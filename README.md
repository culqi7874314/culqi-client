## About
Vue 3 project with composition API, Vite  adn TypeScript with [Culqi](https://culqi.com/) colour based theme


## Project Setup

```
npm install
```
Create a ```.env``` file , add it to the root and include the following variable:
```VITE_API_ENDPOINT=http://localhost:3000```
here you can play with local backend but if you prefer you can configure your environment at your favorite cloud service and put the deployed url.
 

### Compile and Hot-Reload for Development

```
npm run  dev
```

  
### Run Unit Tests with [Vitest](https://vitest.dev/)


```
npm run  test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

 
```
npm run  test:e2e
```

### Lint with [ESLint](https://eslint.org/)

```
npm run  lint
```
In order to navigate to the different menus its required to login with credentials availables at backend side

### Live version [here](https://culqi-client.vercel.app/)
